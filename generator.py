import random

_bene = ["Bene", "Bani", "Butter", "Bendy", "Bonky", "Body", "Bundle", "Beanie", "Bena", "Benaf", "Boobie", "Bubble",
         "Boo", "Beetle", "Baltha", "Brental", "Dumble", "Buff", "Bramble", "Birdie", "Bonus", "Bulba", "Bumble",
         "Buckle", "Blunder", "Bitter", "Brenda", "Blubber", "Bene", "Bench", "Bon", "Broken", "Boppin", "Scissor",
         "Back", "Beezle", "Burger", "Blender", "Billiard", "Guilty", "Bean", "Carrot", "Brody", "Beach", "Baseball",
         "Bed", "Bunsen", "Bengal", "Buda", "Hand", "Burnthis"]
_dict = ["dict", "ster", "cup", "noodle", "hort", "snatch", "up", "bag", "dryl", "fleck", "frog", "bath", "berry",
         "borg", "zar", "floss", "scotch", "dore", "alo", "patch", "beak", "points", "saur", "goof", "bee", "buss",
         "crisp", "dirk", "butt", "dict", "this", "apart", "brick", "stick", "fit", "kick", "itup", "bub", "king",
         "dick", "ball", "verdict", "baf", "quest", "body", "lick", "mitt", "bug", "burner", "tiger", "pest", "picked",
         "land"]
_cumber = ["Cumber", "Crumble", "Cumber", "Crumper", "Cutie", "Cummer", "Catchyour", "Cabbage", "Cucumber", "Camera",
           "Camel", "Caramel", "Crumpet", "Corgi", "Cambridge", "Jazz", "Tima", "Factory", "Scratch-a-", "Crispy",
           "Christmas", "Charmander", "Fiddle", "Cashew", "Cucumper", "Cuddle", "Cramples", "Call", "Comedic",
           "Cunning", "Humper", "Lumber", "Flubber", "Bander", "Cuttle", "Slumber", "Cupboard", "Combyour", "Thunder",
           "Cricket", "Johnny", "Comely", "Custard", "Number", "Lucky", "Cover", "Upto", "Compass", "Chunky", "Candy",
           "Cowden"]
_batch = ["batch", "bench", "snatch", "bunts", "brunch", "bund", "death", "patch", "catch", "neck", "mark", "snacks",
          "pinch", "hands", "fish", "pants", "sniff", "stacks", "time", "stick", "chips", "stache", "crunch",
          "dispatch", "mismatch", "scratch", "finch", "dinck", "latch", "crack", "batch", "belch", "thatch",
          "munch", "bat", "cash", "track", "trap", "bap", "gram", "beath"]


def generate_cumberbatch():
    new_bene = random.choice(_bene)
    new_dict = random.choice(_dict)
    new_cumber = random.choice(_cumber)
    new_batch = random.choice(_batch)
    return "{}{} {}{}".format(new_bene, new_dict, new_cumber, new_batch)
