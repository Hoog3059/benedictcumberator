from flask import Flask, render_template, jsonify
from .generator import generate_cumberbatch, _bene, _dict, _cumber, _batch

app = Flask(__name__)


@app.route('/')
def index():
    p = str(len(_bene) * len(_dict) * len(_cumber) * len(_batch))
    return render_template('index.html', title="Benedict Cumberator", benedict=generate_cumberbatch(),
                           possibilities=p)


@app.route('/request_generator')
def return_benedict():
    benedict_cumberbatch = generate_cumberbatch()
    return jsonify(output=benedict_cumberbatch)


@app.route('/working')
def working():
    p = str(len(_bene) * len(_dict) * len(_cumber) * len(_batch))
    return render_template('working.html', title="Benedict Cumberator - Working", possibilities=p)


@app.route('/contribute')
def contribute():
    return render_template('contribute.html', title="Benedict Cumberator - Contributing")


if __name__ == "__main__":
    app.debug = True
    app.run()
